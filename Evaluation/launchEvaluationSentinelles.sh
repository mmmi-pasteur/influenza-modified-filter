#!/bin/sh
#SBATCH --job-name=EvalSentinelles
#SBATCH -p mmmi
#SBATCH --qos=mmmi
#SBATCH --mem=5000
#SBATCH --array=1-20
#SBATCH -o LogsSentinelles/evaluation_%A_%a.log
#SBATCH -e LogsSentinelles/evaluation_%A_%a.err

param_file="../Tuning/tunedModelsSentinelles.csv"
data_fname=sentinelles20200310.csv
out_folder=Sentinelles20240507

export R_LIBS_USER=/pasteur/zeus/projets/p01/MMMICovid/R/4.0
srun Rscript mainEvaluationSentinelles.R $param_file $SLURM_ARRAY_TASK_ID $data_fname $out_folder

exit 0

