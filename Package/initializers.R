require(tgp) # Latin hypercube sampling

################################################################################
# Deterministic SIR initializer
# Parameters are: prop_infectious0, prop_immune0, R0, gamma
################################################################################
d_sir_init <- function(n_particles, population, lower_bounds, upper_bounds) {
  params <- tgp::lhs(n_particles, cbind(lower_bounds, upper_bounds))
  
  cbind(S = population * (1.0 - params[ , 1] - params[ , 2]),
        I = population * params[ , 1],
        Z = 0,
        R0 = params[ , 3],
        gamma = params[ , 4])
}



