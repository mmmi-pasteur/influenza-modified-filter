################################################################################
# Default regularizer (adds noise to R0)
################################################################################
default_regularizer <- function(state, reg_sd) {
  n_draws <- nrow(state)
  new_R0 <- state[ , "R0"] + rnorm(n_draws, mean = 0.0, sd = reg_sd)

  # Make sure R0 is positive
  to_fix <- new_R0 < 0.0
  new_R0[to_fix] <- 0.1

  state[ , "R0"] <- new_R0

  state
}


