#include <Rcpp.h>
using namespace Rcpp;

// Systematic resampling function. The algorithm is from:
//   A. King et al., Journal of Statistical Software 69 (12), 2016.
// NOTE: it assumes that the weights are normalized to 1.
// [[Rcpp::export]]
IntegerVector systematic_resampling(NumericVector weights) {
  size_t n_elems = weights.size();

  // Cumulative sum
  NumericVector cum_weights = cumsum(weights);

  NumericVector upoints(n_elems);
  upoints[0] = runif(1, 0.0, 1.0 / n_elems)[0];
  for (size_t curr = 1; curr < n_elems; ++curr) {
    upoints[curr] = upoints[0] + curr / double(n_elems);
  }

  IntegerVector inds(n_elems);
  int p = 0;
  for (size_t curr = 0; curr < n_elems; ++curr) {
    while (upoints[curr] > cum_weights[p]) ++p;
    inds[curr] = p + 1; // Indices start at 1 in R!
  }

  return inds;
}
