# Code and data for the article "Integrating information from historical data into mechanistic models for influenza forecasting"
Alessio Andronico, Juliette Paireau and Simon Cauchemez

The repository consists of the following folders:

* Data: containing Sentinelles data plus additional info on the epidemics (`metadata_sentinelles.csv`).
* Package: containing the implementation of the filtering methods.
* Tuning: containing the scripts to tune the parameters of the various models on the training seasons.
* Evaluation: containing the scripts to run the tuned models on the test seasons.

## Tuning
To tune the models:

1. Edit and run `prepareTuningParams<DATASOURCE>.R` (where `<DATASOURCE>` is Sentinelles). The script will generate a csv file containing all combinations of models and parameters that will be explored in `mainTuning<DATASOURCE>.R`.
2. Run `mainTuning<DATASOURCE>.R`. If on the cluster, you can use `launchTuning<DATASOURCE>.sh` to submit the jobs. Note that, in this case, you should create a folder named `Logs<DATASOURCE>` (inside `Tuning`) before launching the scripts as they are: this folder will be used to store the error and log files created by the jobs.
3. Run `mainSelection<DATASOURCE>.R`. You can use `launchSelection<DATASOURCE>.sh` to submit the jobs on the cluster. The script will generate a csv file containing the best parameters (i.e. those corresponding to the highest score) for each model explored in the previous step.

## Evaluation
After the tuning step is completed, you can use `mainEvaluation<DATASOURCE>.R` to retrospectively compute the projections for all test seasons. 
