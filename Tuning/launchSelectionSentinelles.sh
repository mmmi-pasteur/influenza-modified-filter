#!/bin/sh
#SBATCH --job-name=SelectSentinelles
#SBATCH -p mmmi
#SBATCH --qos=mmmi
#SBATCH --mem=16000
#SBATCH -o selectionSentinelles.log
#SBATCH -e selectionSentinelles.err

param_file=tuningParamsSentinelles.csv
out_calib=Sentinelles20240507

export R_LIBS_USER=/pasteur/zeus/projets/p01/MMMICovid/R/4.0
srun Rscript selectModelsSentinelles.R $param_file $out_calib

exit 0
