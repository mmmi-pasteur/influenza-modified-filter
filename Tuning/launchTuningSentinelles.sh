#!/bin/sh
#SBATCH --job-name=TuneSentinelles
#SBATCH -p mmmi
#SBATCH --qos=mmmi
#SBATCH --mem=5000
#SBATCH --array=1-8064%100
#SBATCH -o LogsSentinelles/tuning_%A_%a.log
#SBATCH -e LogsSentinelles/tuning_%A_%a.err

param_file=tuningParamsSentinelles.csv
data_fname=sentinelles20200310.csv
out_calib=Sentinelles20240507

export R_LIBS_USER=/pasteur/zeus/projets/p01/MMMICovid/R/4.0
srun Rscript mainTuningSentinelles.R $param_file $SLURM_ARRAY_TASK_ID $data_fname $out_calib

exit 0

